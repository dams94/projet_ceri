package com.example.ceri_project;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerMusee {

    private static final String TAG = JSONResponseHandlerMusee.class.getSimpleName();

    /**
     * The item
     */
    private Musee_Item item;

    /**
     * Constructor
     * @param item
     */
    public JSONResponseHandlerMusee(Musee_Item item) {
        this.item = item;
    }

    /**
     * Parse to JSON the response of the teams API
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public List<String> readIdsJsonStream(InputStream response) throws IOException {

        List<String> ids = new ArrayList<String>();

        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));

        try {
            readArrayIds(reader,ids);
        } finally {
            reader.close();
        }

        return ids;
    }

    private void readArrayIds(JsonReader reader,List<String> ids) throws IOException {

        reader.beginArray();

        while (reader.hasNext() ) {
            String id = reader.nextString();
            ids.add(id);
            Log.d("ids",id+"");
        }

        reader.endArray();
    }

    public void readJsonStream(InputStream response) throws IOException {

        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));

        try {
            readItems(reader);
        } finally {
            reader.close();
        }
    }


    /**
     * Parse to JSON the response of the last event API
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */


    /**
     * Read the teams
     * @param reader
     * @throws IOException
     */


    public void readItems(JsonReader reader) throws IOException {

        reader.beginObject();

        while (reader.hasNext()) {

            String name = reader.nextName();

            if (name.equals("categories")) {
                readArrayCategories(reader);
            }else if(name.equals("name")){
                item.setName(reader.nextString());
            }else if(name.equals("technicalDetails")){
                readArrayTechnicalDetails(reader, this.item.getTechnicalDetail());
            }else if(name.equals("brand")){
                item.setBrand(reader.nextString());
            }else if(name.equals("year")){
                item.setYear(reader.nextInt());
            }else if(name.equals("thumbnail")){
                item.setThumbnail(reader.nextString());
            }else if(name.equals("description")){
                if(reader.peek() == JsonToken.NULL) {
                    this.item.setDescription("");
                    reader.nextNull();
                }
                else
                    this.item.setDescription(reader.nextString());
            }else if(name.equals("working")){
                item.setWorking(reader.nextBoolean());
            //}else if(name.equals("pictures") && reader.peek() != JsonToken.NULL){
               //readImages(reader, this.item.getPictures());

            }else if(name.equals("timeFrame")){
                readArrayTimeFrame(reader);
            }else{
                reader.skipValue();
            }
        }

        URL thumbnailUrl = WebServiceUrl.buildSearchThumbnail(String.valueOf(item.getIdwb()));
        item.setThumbnail(thumbnailUrl.toString());

        //item.setPictures();

        reader.endObject();
    }
    private void readImages(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String imageUrl = WebServiceUrl.buildSearchImage(this.item.getIdwb(), reader.nextName()).toString();
            String description = reader.nextString();
            //l.add(new ItemImage(description, imageUrl));
        }
        reader.endObject();
    }

    private void readArrayCategories(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext() ) {
            String value = reader.nextString();
            item.getCategorie().add(value);
        }
        reader.endArray();
    }

    private void readArrayTechnicalDetails(JsonReader reader, ArrayList<String> technicalDetail) throws IOException {
        reader.beginArray();
        while (reader.hasNext() ) {
            String value = reader.nextString();
            item.getCategorie().add(value);
        }
        reader.endArray();
    }
    private void readArrayTimeFrame(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext() ) {
            String value = reader.nextString();
            item.getTimeFrame().add(Integer.parseInt(value));
        }
        reader.endArray();
    }

    public Musee_Item getItem() throws MalformedURLException {
        return this.item;
    }

}
