package com.example.ceri_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemSelectedListener {

    public static Musee_Dbhelper dbhelper;
    RecyclerView recyclerView;
    Musee_Adapter adapter;
    EditText recherrche;
    public static SwipeRefreshLayout swipe;
    Spinner spinner;
    Musee_Adapter recyclerViewAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // swipe = findViewById(R.id.swipe);
        //swipe.setOnRefreshListener(this);
        recherrche=findViewById(R.id.recher);

        recherrche.addTextChangedListener(new TextWatcher(){
            @Override
            public void onTextChanged(CharSequence s, int start, int before,int count) {
                populateView();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        dbhelper=new Musee_Dbhelper(this);

       // this.dbhelper.populate();
          this.recyclerView = findViewById(R.id.RecyclerView);
        MyAsyncTask myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute();

         populateView();
        dbhelper.deleteAllMusses();


    }

    public void populateView(){

        String search=recherrche.getText().toString();
        spinner=(Spinner) findViewById(R.id.triee);
 
        ArrayAdapter<CharSequence> adapter= ArrayAdapter.createFromResource(this, R.array.triee, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String search = recherrche.getText().toString();
        String affichage = parent.getItemAtPosition(position).toString();
        if(affichage.equalsIgnoreCase("Ordre Chronologique")) {
            recyclerViewAdaptor = new Musee_Adapter(this, dbhelper.getAllMuseeChronologique(search));
            recyclerView.setAdapter(recyclerViewAdaptor);
            recyclerView.setItemViewCacheSize(0);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        }
        if(affichage.equalsIgnoreCase("Ordre Alphabetique")) {
            recyclerViewAdaptor = new Musee_Adapter(this, dbhelper.getAllMusses(search));
            recyclerView.setAdapter(recyclerViewAdaptor);
            recyclerView.setItemViewCacheSize(0);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public class MyAsyncTask extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONResponseHandlerMusee jsonHandler = new JSONResponseHandlerMusee(null);
            Musee_Item item = new Musee_Item();
            try {
                dbhelper.deleteAllMusses();
                List<String> ids = jsonHandler.readIdsJsonStream(WebServiceUrl.buildSearchIds().openStream());
                for(String id : ids){

                    item.setIdwb(id);
                    JSONResponseHandlerMusee jh = new JSONResponseHandlerMusee(item);
                    jh.readJsonStream(WebServiceUrl.buildSearchItem(id).openStream());

                    item = jh.getItem();


                   dbhelper.addMusses(item);


                }

            } catch (IOException e) {
                e.printStackTrace();
            }




            return "rien";
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            populateView();

        }


    }

    @Override
    public void onRefresh() {

    }

}

