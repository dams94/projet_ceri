package com.example.ceri_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class Musee_Activity extends AppCompatActivity {
    private TextView txtName, txtBrand, txtYear, txtCategories, txtDesc, txtDetails, txtTime_frime;

    private ImageView ivThumbnail;
    private SliderAdapterExample adapter;
    private SliderView sliderView;
    public static Context c;
    private JSONResponseHandlerMusee responseHandlerMusee;

    private Musee_Item musee_Item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musee);

      SliderView sliderView = findViewById(R.id.itemPicturesSliderView);

         adapter = new SliderAdapterExample(this);

        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();


        this.musee_Item = getIntent().getParcelableExtra(musee_Item.TAG);

        txtName = findViewById(R.id.Name);
        ivThumbnail = findViewById(R.id.Thumbnail);
        txtBrand = findViewById(R.id.Brand);
        txtYear = findViewById(R.id.Year);
        txtCategories = findViewById(R.id.Categories);
        txtDesc = findViewById(R.id.Desc);
        txtDetails = findViewById(R.id.Details);
        txtTime_frime = findViewById(R.id.Time_frime);



        this.updateView();

    }

    private void updateView() {

        this.txtName.setText(this.musee_Item.getName());
        this.txtDesc.setText(this.musee_Item.getDescription());
        this.txtBrand.setText(this.musee_Item.getBrand());
       this.txtYear.setText(String.valueOf(this.musee_Item.getYear()));
       this.txtTime_frime.setText(String.valueOf(this.musee_Item.getTimeFrame()));
      this.txtCategories.setText(String.valueOf(this.musee_Item.getCategorie()));
       this.txtDetails.setText(String.valueOf(this.musee_Item.getTechnicalDetail()));

       Picasso.with(this).load(musee_Item.getThumbnail()).placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(ivThumbnail, new com.squareup.picasso.Callback(
                ) {
                    @Override
                    public void onSuccess() {
                    }
                    @Override
                    public void onError(){

                    }
                });

        String details = "Détails techniques : \n";

        String categories = "";
        if(!this.musee_Item.getCategorie().isEmpty()) {
            int i = 0;
            for( ; i < this.musee_Item.getCategorie().size() - 1 ; ++i)
                categories += this.musee_Item.getCategorie().get(i) + ", ";
            categories += this.musee_Item.getCategorie().get(i);
        }
        this.txtCategories.setText(categories);



    }

    }

