package com.example.ceri_project;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Musee_Adapter extends RecyclerView.Adapter<Musee_Adapter.ViewHolder> {
    private Context context;

    public List<Musee_Item> catalog;

    public Musee_Adapter(Context context, List<Musee_Item> catalog) {
        this.catalog=catalog;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater myInflater = LayoutInflater.from(context);
        View view = myInflater.inflate(R.layout.row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        Musee_Item musee_item = catalog.get(position);
        Log.d("api img","url"+musee_item.getThumbnail());
        Picasso.with(context).load(musee_item.getThumbnail()).placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(holder.image, new com.squareup.picasso.Callback(
                ) {
                    @Override
                    public void onSuccess() {
                    }
                    @Override
                    public void onError(){

                    }
                });


        holder.MuseName.setText(musee_item.getName());
        holder.MuseeBrand.setText(musee_item.getBrand());


        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(context, Musee_Activity.class);
                 mIntent.putExtra(Musee_Item.TAG, catalog.get(position));
                context.startActivity(mIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return catalog.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout parentLayout;

        ImageView image;
        TextView MuseName;
        TextView MuseeBrand;


        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            parentLayout = itemView.findViewById(R.id.row);

            image = itemView.findViewById(R.id.image);
            MuseName = itemView.findViewById(R.id.MuseeName);
            MuseeBrand = itemView.findViewById(R.id.MuseeBrand);

        }
    }
}

