package com.example.ceri_project;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class Musee_Dbhelper extends SQLiteOpenHelper {
    private static final String TAG = Musee_Dbhelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "musse.db";
    public static final String TABLE_NAME = "musse";

    public static final String _ID = "_id";
    public static final String COLUMN_WEB_ID = "web_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_DESCRIPTION = "descrisption";
    public static final String COLUMN_CATEGORIE = "categorie";

    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_THUMBNAIL = "thumbnail";
    public static final String COLUMN_PICTURES = "pictures";
    public static final String COLUMN_TIMEFRAME = "timeframe";
    public static final String COLUMN_TECHNICALDETAIL = "technicaldetail";
    public static final String COLUMN_LASTUPDATE = "lastupdate";

    public Musee_Dbhelper(Context context) { super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        Log.d(TAG, "onCreate: called");
        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_WEB_ID + " TEXT NOT NULL, " +
                COLUMN_NAME + " TEXT NOT NULL, " +
                COLUMN_BRAND + " TEXT, " +
                COLUMN_THUMBNAIL + " TEXT, " +
                COLUMN_YEAR + " INTEGER, " +
                COLUMN_TIMEFRAME + " TEXT, " +
                COLUMN_CATEGORIE + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_PICTURES + " TEXT, " +
                COLUMN_TECHNICALDETAIL + "TEXT, " +
                COLUMN_LASTUPDATE+ " TEXT, " +
                // To assure the application have just one item entry per
                // item name and brand, it's created a UNIQUE
                " UNIQUE (" + COLUMN_NAME + ", " +
                COLUMN_BRAND + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_BOOK_TABLE);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void deleteAllMusses(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, "", null);
        db.close();
    }

    private ContentValues fill(Musee_Item musee){

        ContentValues values =new ContentValues();
        Gson gson = new Gson();

        values.put(COLUMN_WEB_ID, musee.getIdwb());
        values.put(COLUMN_NAME, musee.getName());
        values.put(COLUMN_BRAND, musee.getBrand());
        values.put(COLUMN_THUMBNAIL, musee.getThumbnail());
        values.put(COLUMN_YEAR, musee.getYear());
        values.put(COLUMN_TIMEFRAME, gson.toJson(musee.getTimeFrame()));
        values.put(COLUMN_CATEGORIE, gson.toJson(musee.getCategorie()));
        values.put(COLUMN_DESCRIPTION, musee.getDescription());
        values.put(COLUMN_PICTURES, gson.toJson(musee.getPictures()));
        values.put(COLUMN_TECHNICALDETAIL, gson.toJson(musee.getTechnicalDetail()));
        values.put(COLUMN_LASTUPDATE, musee.getLastUpdate());
        return values;
    }



    /**
     * Adds a new item
     * @return  true if the item was added to the table ; false otherwise (case when the pair (name, brand) is
     * already in the data base)
     */
    public boolean addMusses(Musee_Item musee) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(musee);

        Log.d(TAG, "adding: "+ musee.getName()+" with id="+musee.getId());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (name, brand)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }
    /**
     * Updates the information of an musses inside the data base
     * @return the number of updated rows
     */
    public int updateMusses(Musee_Item musee) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(musee);

        // updating row
        return db.updateWithOnConflict(TABLE_NAME, values, _ID + " = ?",
                new String[] { String.valueOf(musee.getId()) }, CONFLICT_IGNORE);
    }
    public Cursor fetchAllMesses(String seach) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                COLUMN_NAME +" LIKE '%"+seach+"%' ", null, null, null, COLUMN_NAME +" ASC", null);

        Log.d(TAG, "call fetchAllItems()");



        if (cursor != null) {
            cursor.moveToFirst();
        }

        return cursor;
    }
    public Cursor fetchAllMessesChronologie(String seacher) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_YEAR +" ASC", null);

        Log.d(TAG, "call fetchAllItems()");



        if (cursor != null) {
            cursor.moveToFirst();
        }

        return cursor;
    }

    public void deleteMusses(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    /**
     * Returns a list on all the teams of the data base
     */
    public List<Musee_Item> getAllMusses(String seach) {

        List<Musee_Item> res = new ArrayList<>();

        Cursor cursor = fetchAllMesses(seach);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Musee_Item musee = this.cursorToMusse(cursor);
                res.add(musee);
                cursor.moveToNext();
            }
        }

        return res;
    }

    public List<Musee_Item>  getAllMuseeChronologique(String seach) {

        List<Musee_Item> res = new ArrayList<>();

        Cursor cursor = fetchAllMessesChronologie(seach);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Musee_Item musee = this.cursorToMusse(cursor);
                res.add(musee);
                cursor.moveToNext();
            }
        }

        return res;
    }



    public Musee_Item cursorToMusse(Cursor cursor){

        Gson gson = new Gson();
        ArrayList<Integer> timeFrames = gson.fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_TIMEFRAME)), ArrayList.class);
        ArrayList<String> categories = gson.fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORIE)), ArrayList.class);
        ArrayList<String> pictures = gson.fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_PICTURES)), ArrayList.class);
        ArrayList<String> technicalDetails = gson.fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_TECHNICALDETAIL)), ArrayList.class);

        Musee_Item musee = new Musee_Item(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_WEB_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_BRAND)),
                cursor.getString(cursor.getColumnIndex(COLUMN_THUMBNAIL)),
                cursor.getInt(cursor.getColumnIndex(COLUMN_YEAR)),
                timeFrames,
                categories,
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                pictures,
                technicalDetails,
                cursor.getString(cursor.getColumnIndex(COLUMN_LASTUPDATE))
        );

        return musee;
    }
    public Musee_Item getMusses(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                _ID+" = ?", new String[]{String.valueOf(id)}, null, null, null, "1");
        if (cursor != null)
            cursor.moveToFirst();
        else
            return null;
        return this.cursorToMusse(cursor);
    }

   /* public Musee_Item getMusses(String name, String brand) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                COLUMN_NAME+" = ? and "+COLUMN_BRAND+" = ?", new String[]{name, brand}, null, null, null, "1");
        if (cursor != null)
            cursor.moveToFirst();
        else
            return null;
        return this.cursorToMusse(cursor);œ
    }
*/
}

