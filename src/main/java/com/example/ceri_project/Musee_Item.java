package com.example.ceri_project;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Musee_Item implements Parcelable {
    private long id;  // used for the _id column of the db helper
    public int year;
    private String name;
    public String brand;
    public boolean working;
    private String idwb;
    public String description;
    private ArrayList<String> categorie;
    private ArrayList<Integer> timeFrame;
    private ArrayList<String> technicalDetail;
    private ArrayList<String>pictures;
    public String lastUpdate;
    public String thumbnail;




    public static final String TAG = Musee_Item.class.getSimpleName();


    public static final Parcelable.Creator<Musee_Item> CREATOR = new Parcelable.Creator<Musee_Item>() {


        @Override
        public Musee_Item createFromParcel(Parcel source) {
            return new Musee_Item(source);
        }

        @Override
        public Musee_Item[] newArray(int size) {
            return new Musee_Item[size];
        }
    };
    public Musee_Item() {
        this.idwb = new String("");
        this.name = new String("");
        this.thumbnail = new String("");
        this.brand = new String("");
        this.description = new String("");
        this.lastUpdate = new String("");
        this.year = 7777;
        this.timeFrame = new ArrayList<>();
        this.categorie = new ArrayList<>();
        this.pictures = new ArrayList<>();
        this.technicalDetail = new ArrayList<>();
    }
/*
    public Musee_Item(Musee_Item musee) {
        this.id = musee.id;
        this.idwb = new String(musee.idwb);
        this.name = new String(musee.name);
        this.thumbnail = new String(musee.thumbnail);
        this.brand = new String(musee.brand);
        this.year = musee.year;
        this.timeFrame = new ArrayList<>(musee.timeFrame);
        this.categorie = new ArrayList<>(musee.categorie);
        this.description = new String(musee.description);
        this.pictures = new ArrayList<>(musee.pictures);
        this.technicalDetail = new ArrayList<>(musee.technicalDetail);
        this.lastUpdate = new String(musee.lastUpdate);
    }*/

    public Musee_Item(long id, String idwb, String name, String brand,String thumbnail, int year,
                  ArrayList<Integer> timeFrame, ArrayList<String> categories, String desc,
                      ArrayList<String> pictures, ArrayList<String> technicalDetails, String lastUpdate)
    {
        this.id = id;
        this.idwb=idwb;
        this.name = name;
        this.brand = brand;
        this.thumbnail=thumbnail;
        this.year = year;
        this.timeFrame = timeFrame;
        this.categorie = categories;
        this.description = desc;
        this.pictures = pictures;
        this.technicalDetail = technicalDetails;
        this.lastUpdate = lastUpdate;
    }


    public String getDescription(){
        return this.description;
    }
    public long  getId(){ return id; }
    public String getLastUpdate(){return lastUpdate;}
    public String getThumbnail() { return this.thumbnail; }
    public void setThumbnail(String thumbnail) { this.thumbnail = thumbnail; }

    public String getName(){
        return name;
    }
    public String getIdwb(){return idwb;}
    public String getBrand(){
        return brand;
    }
    public ArrayList<String>  getCategorie(){return categorie;}
    public  ArrayList<String> getTechnicalDetail(){return technicalDetail;}
    public ArrayList<String > getPictures(){return pictures;}
    public int getYear(){return year;}
    public void setWorking(boolean working) {
        this.working = working;
    }

    public boolean isWorking(){return working;}
    public ArrayList<Integer>  getTimeFrame(){return timeFrame;}
    public void setIdwb(String idwb){this.idwb=idwb;}
    public void setId (long id){this.id=id;}
    public void setYear(int year){this.year=year;}
    public void setName(String name){
        this.name=name;
    }
    public void setBrand(String brand){this.brand=brand;}
    public void setDescription(String description){this.description=description;}
    public void setLastUpdate() {
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
        this.lastUpdate = dateFormat.format(currentTime);
    }
    public void setCategorie(ArrayList<String>  categorie){this.categorie=categorie;}
    public void setTimeFrame(ArrayList<Integer>  timeFrame){this.timeFrame=timeFrame;}
    public void setTechnicalDetail(ArrayList<String>  technicalDetail){this.technicalDetail=technicalDetail;}
    public void setPictures(ArrayList<String>  pictures){this.pictures=pictures;}


    @Override
    public String toString() {
        return this.name+", "+this.brand +" ("+this.year +")" + this.idwb +"("+this.technicalDetail+")";
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(thumbnail);
        dest.writeString(brand);
        dest.writeInt(year);
        dest.writeList(timeFrame);
        dest.writeList(categorie);
        dest.writeString(description);
        dest.writeList(pictures);
        dest.writeList(technicalDetail);
        dest.writeString(lastUpdate);



    }

    public Musee_Item(Parcel in){

        this.id=in.readLong();
        this.name=in.readString();
        this.thumbnail=in.readString();
        this.brand=in.readString();
        this.year=in.readInt();
        this.timeFrame=in.readArrayList(Integer.class.getClassLoader());
        this.categorie=in.readArrayList(String.class.getClassLoader());
        this.description=in.readString();
        this.pictures = in.readArrayList(String.class.getClassLoader());
        this.technicalDetail = in.readArrayList(String.class.getClassLoader());

    }
}

