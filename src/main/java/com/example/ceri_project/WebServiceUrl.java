package com.example.ceri_project;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {
    // constant string used as s parameter for lookuptable
    private static final String CUR_YEAR = "1920";

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH);
        return builder;
    }

   // https://demo-lia.univ-avignon.fr/cerimuseum/ids
    private static final String SEARCH_ID = "ids";
    private static final String SEARCH_ITEM = "items";
    private static final String SEARCH_THUMBNAIL = "thumbnail";
    private static final String SEARCH_CATEGORIES = "categories";
    private static final String SEARCH_TECHNICALDETAILS = "technicalDetails";

    // Build URL to get information for a specific item
    public static URL buildSearchIds() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ID);
        URL url = new URL(builder.build().toString());
        return url; // https://demo-lia.univ-avignon.fr/cerimuseum/ids/
    }

    public static URL buildSearchItem(String itemWebId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEM)
                .appendPath(itemWebId);
        URL url = new URL(builder.build().toString());
        return url; // https://demo-lia.univ-avignon.fr/cerimuseum/items/ids
    }
    private static final String SEARCH_IMAGE = "images";
    public static URL buildSearchImage(String itemWebId, String imageName) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEM)
                .appendPath(itemWebId)
                .appendPath(SEARCH_IMAGE)
                .appendPath(imageName);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Build URL to get a specific image from an item
    public static URL buildSearchThumbnail(String itemWebId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEM)
                .appendPath(itemWebId)
                .appendPath(SEARCH_THUMBNAIL);
        URL url = new URL(builder.build().toString());
        return url;
    }



}
